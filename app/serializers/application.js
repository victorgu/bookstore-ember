import DS from 'ember-data';
import { singularize } from 'ember-inflector';

export default DS.JSONAPISerializer.extend({
  payloadKeyFromModelName(modelName) {
    //return singularize(capitalize(modelName));
    return singularize(modelName);
  }
});
